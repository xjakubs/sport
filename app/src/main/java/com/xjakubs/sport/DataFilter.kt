package com.xjakubs.sport

enum class DataFilter(val value: Int) {
    LOCAL(0), REMOTE(1), BOTH(2)
}

