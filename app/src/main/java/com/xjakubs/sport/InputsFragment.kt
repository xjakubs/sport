package com.xjakubs.sport


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.xjakubs.sport.databinding.InputsFragmentBinding

class InputsFragment : Fragment() {

    private val viewModel: InputsViewModel by viewModels()

    private var _binding: InputsFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = InputsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.name.observe(
            viewLifecycleOwner,
            { newName -> binding.inputsNameEditText.setText(newName) })
        viewModel.place.observe(
            viewLifecycleOwner,
            { newPlace -> binding.inputsPlaceEditText.setText(newPlace) })
        viewModel.duration.observe(
            viewLifecycleOwner,
            { newDuration -> binding.inputsDurationEditText.setText(newDuration.toString()) })
        viewModel.storageLocal.observe(
            viewLifecycleOwner,
            { newStorage ->
                if (newStorage) {
                    binding.inputsStorageRadioGroup.check(R.id.radioStorageLocal)
                } else {
                    binding.inputsStorageRadioGroup.check(R.id.radioStorageRemote)
                }
            })

        binding.submit.setOnClickListener { onSubmitRecord() }
        binding.inputsStorageRadioGroup.setOnCheckedChangeListener { _, i ->
            when (i) {
                R.id.radioStorageLocal -> viewModel.setStorageLocal(true)
                R.id.radioStorageRemote -> viewModel.setStorageLocal(false)
            }
        }
    }


    private fun onSubmitRecord() {
        var isCorrect = true
        val name = binding.inputsNameEditText.text.toString()
        isCorrect = setErrorTextField(binding.inputsNameTextField, name.isEmpty()) && isCorrect
        val place = binding.inputsPlaceEditText.text.toString()
        isCorrect = setErrorTextField(binding.inputsPlaceTextField, place.isEmpty()) && isCorrect
        val duration = binding.inputsDurationEditText.text.toString().toLong()
        isCorrect = setErrorTextField(binding.inputsDurationTextField, duration == 0L) && isCorrect

        if (isCorrect) {
            var saved = false
            val newRecord = SportData(name, place, duration)

            if (viewModel.storageLocal.value == true) {

                if (JsonReadWrite(
                        requireContext(),
                        resources.getString(R.string.local_json_file_name)
                    ).saveJsonFile(newRecord)
                ) {
                    saved = true
                } else {
                    Toast.makeText(requireContext(), R.string.errorFileFail, Toast.LENGTH_SHORT)
                        .show()
                }


            } else {
                //TODO REMOTE
                if (JsonReadWrite(
                        requireContext(),
                        resources.getString(R.string.remote_json_file_name)
                    ).saveJsonFile(newRecord)
                ) {
                    saved = true
                } else {
                    Toast.makeText(requireContext(), R.string.errorFileFail, Toast.LENGTH_SHORT)
                        .show()
                }
            }
            if (saved) {
                val action =
                    InputsFragmentDirections.actionInputsFragmentToResultsFragment(storage = viewModel.storageLocal.value == true)
                findNavController().navigate(action)
                viewModel.reset()
            }
        }

    }


    private fun setErrorTextField(txtInLayout: TextInputLayout, error: Boolean): Boolean {
        if (error) {
            txtInLayout.isErrorEnabled = true
            txtInLayout.error = getString(R.string.inputs_empty_field)
            return false
        }
        txtInLayout.isErrorEnabled = false
        return true
    }

}

