package com.xjakubs.sport

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InputsViewModel : ViewModel() {

    private val _name = MutableLiveData<String>()
    val name: LiveData<String>
        get() = _name

    private val _place = MutableLiveData<String>()
    val place: LiveData<String>
        get() = _place

    private val _duration = MutableLiveData<Long>(0)
    val duration: LiveData<Long>
        get() = _duration

    private val _storageLocal = MutableLiveData(true)
    val storageLocal: LiveData<Boolean>
        get() = _storageLocal

    fun reset() {
        _name.value = ""
        _place.value = ""
        _duration.value = 0L
        _storageLocal.value = true
    }

    fun setStorageLocal(enabled: Boolean) {
        _storageLocal.value = enabled
    }
}