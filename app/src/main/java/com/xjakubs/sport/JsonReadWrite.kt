package com.xjakubs.sport

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.IOException

class JsonReadWrite(context: Context, fileName: String) {

    private val gson = Gson()
    private val gsonPretty = GsonBuilder().setPrettyPrinting().create()
    private var file = File(context.filesDir.path + "/" + fileName)

    fun saveJsonFile(newRecord: SportData): Boolean {
        var sportDataList: MutableList<SportData> = ArrayList()
        try {
            if (!file.createNewFile()) {
                sportDataList = readJsonFile()
            }

            sportDataList.add(newRecord)
            val jsonString: String = gsonPretty.toJson(sportDataList)
            file.writeText(jsonString)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return false
        } catch (e: Exception) {
            println(e.message)
            return false
        }

        return true
    }

    fun readJsonFile(): MutableList<SportData> {
        if (file.exists()) {
            val jsonFileString = getJsonDataFromFile()
            val sportDataType = object : TypeToken<MutableList<SportData>>() {}.type
            return gson.fromJson(jsonFileString, sportDataType) ?: ArrayList()
        }
        return ArrayList()
    }

    private fun getJsonDataFromFile(): String? {
        val jsonString: String
        try {
            jsonString = file.bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}

