package com.xjakubs.sport

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class ResultsAdapter(
    private val context: Context,
    dataList: List<SportData>?,
    dataAllList: List<SportDataAll>?
) :
    RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder>() {

    private var dataList: List<SportData> = dataList ?: ArrayList()
    private var dataAllList: List<SportDataAll> = dataAllList ?: ArrayList()
    private var twoList: Boolean = false

    constructor(context: Context, data: List<SportData>) : this(context, data, null) {
        twoList = false
    }

    constructor(context: Context, data: List<SportDataAll>, two: Boolean) : this(
        context,
        null,
        data
    ) {
        twoList = two
    }

    class ResultsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.dataName)
        val place: TextView = view.findViewById(R.id.dataPlace)
        val duration: TextView = view.findViewById(R.id.dataDuration)
        val local: CardView = view.findViewById(R.id.itemCard)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {
        val layout = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_card, parent, false)

        return ResultsViewHolder(layout)
    }

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        if (twoList) {
            val item = dataAllList[position]
            holder.name.text = item.name
            holder.place.text = item.place
            holder.duration.text = context.getString(R.string.results_duration_min, item.duration)

            if (!item.local) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.local.setCardBackgroundColor(
                        context.resources.getColor(
                            R.color.gray,
                            null
                        )
                    )
                }
            }
        } else {
            val item = dataList[position]
            holder.name.text = item.name
            holder.place.text = item.place
            holder.duration.text = context.getString(R.string.results_duration_min, item.duration)
        }

    }

    override fun getItemCount(): Int {
        if (twoList) {
            return dataAllList.size
        }
        return dataList.size
    }

}
