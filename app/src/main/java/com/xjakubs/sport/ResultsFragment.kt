package com.xjakubs.sport

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xjakubs.sport.R.menu.layout_menu
import com.xjakubs.sport.databinding.ResultsFragmentBinding

class ResultsFragment : Fragment() {

    companion object {
        const val STORAGE = "storage"
    }

    private val viewModel: ResultsViewModel by viewModels()

    private var _binding: ResultsFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    private var onStartStorage = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        arguments?.let {
            if (it.getBoolean(STORAGE)) {
                onStartStorage = DataFilter.LOCAL.value
            } else {
                onStartStorage = DataFilter.REMOTE.value
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ResultsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        if (viewModel.adapter.value == null) {
            setAdapter()
        } else {
            recyclerView.adapter = viewModel.adapter.value
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(layout_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter_results -> {
//                viewModel.storage.value?.let { showFilterDialog(it) }
                showFilterDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showFilterDialog() {

        val item = viewModel.storage.value?.value ?: onStartStorage
        val filterOptions = arrayOf(
            getString(R.string.results_filter_locale),
            getString(R.string.results_filter_remote),
            getString(R.string.results_filter_all)
        )
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.action_filter_results)
        builder.setSingleChoiceItems(
            filterOptions,
            item,
            DialogInterface.OnClickListener { dialogInterface, i ->
                when (i) {
                    0 -> viewModel.setStorage(DataFilter.LOCAL)
                    1 -> viewModel.setStorage(DataFilter.REMOTE)
                    2 -> viewModel.setStorage(DataFilter.BOTH)
                }
                setAdapter()
                dialogInterface.dismiss()
            })
        builder.setNegativeButton(
            R.string.results_filter_Cancel,
            DialogInterface.OnClickListener { dialogInterface, _ -> dialogInterface.dismiss() })
        builder.show()
    }

    private fun setAdapter() {
        val context: Context = requireContext()
        var storage = DataFilter.LOCAL
        when (onStartStorage) {
            0 -> storage = DataFilter.LOCAL
            1 -> storage = DataFilter.REMOTE
        }
        val adapter: ResultsAdapter
        when (viewModel.storage.value ?: storage) {
            DataFilter.LOCAL -> {
                adapter = ResultsAdapter(context, prepareData(R.string.local_json_file_name))

            }
            DataFilter.REMOTE -> {
                adapter = ResultsAdapter(context, prepareData(R.string.remote_json_file_name))
            }
            DataFilter.BOTH -> {
                adapter = ResultsAdapter(
                    context,
                    prepareData(R.string.local_json_file_name, R.string.remote_json_file_name),
                    true
                )
            }
        }
        viewModel.adapter.value = adapter
        recyclerView.adapter = adapter
    }

    private fun prepareData(file: Int): List<SportData> {
        val data: List<SportData> = JsonReadWrite(
            requireContext(),
            resources.getString(file)
        ).readJsonFile()
        return data.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER, { it.name }))
    }

    private fun prepareData(fileLocal: Int, fileRemote: Int): List<SportDataAll> {
        val localData: List<SportData> = JsonReadWrite(
            requireContext(),
            resources.getString(fileLocal)
        ).readJsonFile()
        val remoteData: List<SportData> = JsonReadWrite(
            requireContext(),
            resources.getString(fileRemote)
        ).readJsonFile()
        val data: MutableList<SportDataAll> = mutableListOf()
        localData.forEach { sportData ->
            data.add(
                SportDataAll(
                    true,
                    sportData.name,
                    sportData.place,
                    sportData.duration
                )
            )
        }
        remoteData.forEach { sportData ->
            data.add(
                SportDataAll(
                    false,
                    sportData.name,
                    sportData.place,
                    sportData.duration
                )
            )
        }
        return data.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER, { it.name }))
    }

}