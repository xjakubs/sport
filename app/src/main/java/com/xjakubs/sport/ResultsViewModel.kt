package com.xjakubs.sport

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ResultsViewModel : ViewModel() {

    private val _storage = MutableLiveData<DataFilter>(null)
    val storage: LiveData<DataFilter>
        get() = _storage

    fun setStorage(value: DataFilter) {
        _storage.value = value
    }

    private val _adapter = MutableLiveData<ResultsAdapter>(null)
    val adapter: MutableLiveData<ResultsAdapter>
        get() = _adapter
}