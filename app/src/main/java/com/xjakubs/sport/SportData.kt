package com.xjakubs.sport

open class SportData(open val name: String, open val place: String, open val duration: Long) {

}

data class SportDataAll(
    val local: Boolean,
    override var name: String,
    override var place: String,
    override var duration: Long
) : SportData(name, place, duration) {
}

